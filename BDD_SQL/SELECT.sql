--------**Roles**--------

SELECT * FROM pg_roles;

--------**Tables**--------

SELECT * FROM Pays;
SELECT * FROM Ville;
SELECT * FROM Logement;
SELECT * FROM Prix;
SELECT * FROM Circuit;
SELECT * FROM Etape;
SELECT * FROM Accompagnateur;
SELECT * FROM Activite;
SELECT * FROM PlanActivite;
SELECT * FROM Equipments;
SELECT * FROM Reservation;
SELECT * FROM Facture;
SELECT * FROM Frais;
SELECT * FROM Commentaire;
SELECT * FROM Consulter;
SELECT * FROM Chercher;

--------**Vue**--------

SELECT * FROM plus_consulte;
SELECT * FROM plus_cherche;
SELECT * FROM nb_reservations;
SELECT * FROM circuits_reserves_age;
