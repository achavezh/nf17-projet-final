-------------*CREATION DE LA BASE(ici le superuser est postgres)*-------------

CREATE DATABASE agence WITH OWNER postgres;

-------------*ROLES*-------------
    ---ADMIN(gestionneur de la base)---

CREATE ROLE admin NOSUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;

    ---UTILISATEUR(Clients)---
    
CREATE ROLE utilisateur NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

    ---ACCOMP(Accompagnateurs)---
    
CREATE ROLE accomp NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

-------------*CONNECTION A LA BASE*-------------

GRANT  CONNECT ON DATABASE agence TO admin;
GRANT  CONNECT ON DATABASE agence TO utilisateur;
GRANT  CONNECT ON DATABASE agence TO accomp;

-------------*DROITS*-------------

GRANT ALL ON ALL TABLES IN SCHEMA public TO admin;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO utilisateur;
GRANT INSERT ON Client TO utilisateur;
GRANT UPDATE ON Client TO utilisateur;
GRANT INSERT ON Reservation TO utilisateur;
GRANT INSERT ON Commentaire TO utilisateur;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO accomp;
GRANT INSERT ON Accompagnateur TO accomp;
GRANT UPDATE ON Accompagnateur TO accomp;

-------------*CREATION DES TABLES*-------------

CREATE TABLE Pays(
code_pays VARCHAR(2) PRIMARY KEY,
nom_pays VARCHAR NOT NULL);

CREATE TABLE Ville(
id_ville SERIAL PRIMARY KEY,
nom_ville VARCHAR NOT NULL,
code_pays VARCHAR(2) NOT NULL,
FOREIGN KEY (code_pays) REFERENCES Pays(code_pays));

CREATE TABLE Logement(
id_logement SERIAL PRIMARY KEY,
id_ville INTEGER NOT NULL,
type VARCHAR NOT NULL,
CHECK (type = 'hotel' OR type = 'chez_particulier'),
FOREIGN KEY (id_ville) REFERENCES Ville(id_ville));

CREATE TABLE Prix(
id_prix SERIAL PRIMARY KEY,
montant FLOAT,
type VARCHAR NOT NULL,
CHECK(type='standard' OR type='eleve'),
CHECK(montant > 0));

CREATE TABLE Circuit(
id_circuit SERIAL PRIMARY KEY,
date_depart DATE NOT NULL,
duree INTEGER,
nb_participantes INTEGER,
niveau_difficulte INTEGER, 
id_prix INTEGER NOT NULL,
CHECK (duree>0 AND nb_participantes>0),
FOREIGN KEY (id_prix) REFERENCES Prix(id_prix));

CREATE TABLE Etape(
id_etape SERIAL PRIMARY KEY,
id_circuit INTEGER NOT NULL,
id_logement INTEGER,
transport VARCHAR NOT NULL,
FOREIGN KEY (id_circuit) REFERENCES Circuit(id_circuit),
FOREIGN KEY (id_logement) REFERENCES Logement(id_logement),
CHECK(transport='avion' OR transport='bus' OR transport='voiture'));

CREATE TABLE Accompagnateur(
id_accompagnateur SERIAL PRIMARY KEY,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL, 
montant_frais FLOAT NOT NULL,
CHECK(montant_frais > 0));

CREATE TABLE Activite(
id_activite SERIAL PRIMARY KEY, 
type_activite VARCHAR UNIQUE NOT NULL);

CREATE TABLE PlanActivite(
id_planactivite INTEGER PRIMARY KEY,
id_activite INTEGER NOT NULL, 
id_etape INTEGER NOT NULL,
id_accompagnateur INTEGER NOT NULL,
date_initial DATE NOT NULL,
date_final DATE NOT NULL,
CHECK (date_initial <= date_final),
FOREIGN KEY(id_activite) REFERENCES Activite(id_activite),
FOREIGN KEY(id_etape) REFERENCES Etape(id_etape),
FOREIGN KEY(id_accompagnateur) REFERENCES Accompagnateur(id_accompagnateur));

CREATE TABLE Equipments(
id_equipment SERIAL PRIMARY KEY,
id_activite INTEGER NOT NULL,
nom_equipment VARCHAR NOT NULL,
FOREIGN KEY(id_activite) REFERENCES Activite(id_activite));

CREATE TABLE Client(
id_client SERIAL PRIMARY KEY,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL,
adresse VARCHAR NOT NULL,
telephone INTEGER NOT NULL,
age INTEGER NOT NULL,
CHECK(age>=18));

CREATE TABLE Reservation(
id_reservation SERIAL PRIMARY KEY, 
id_client INTEGER NOT NULL,
id_circuit INTEGER NOT NULL,
date_reservation DATE NOT NULL,
nb_accompagnates INTEGER,
status VARCHAR NOT NULL, 
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
CHECK(status='reserve' OR status='partiellement_paye' OR status='totallement_paye'),
CHECK (nb_accompagnates >= 0));

CREATE TABLE Facture(
id_facture SERIAL PRIMARY KEY,
id_client INTEGER NOT NULL,
FOREIGN KEY(id_client) REFERENCES Client(id_client)
);

CREATE TABLE Frais(
id_frais SERIAL PRIMARY KEY,
id_reservation INTEGER NOT NULL,
id_facture INTEGER NOT NULL,
type VARCHAR NOT NULL,
FOREIGN KEY(id_reservation) REFERENCES Reservation(id_reservation),
FOREIGN KEY(id_facture) REFERENCES Facture(id_facture),
CHECK (type='location' OR type='reservation')
);

CREATE TABLE Commentaire(
id_circuit INTEGER NOT NULL,
id_client INTEGER NOT NULL,
commentaire VARCHAR,
notation INTEGER,
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
PRIMARY KEY(id_circuit, id_client),
CHECK(notation>=0 AND notation<=5)
);

CREATE TABLE Consulter(
id_circuit INTEGER NOT NULL,
id_client INTEGER NOT NULL,
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
PRIMARY KEY(id_circuit, id_client)
);

CREATE TABLE Chercher(
id_circuit INTEGER NOT NULL,
id_client INTEGER NOT NULL,
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
PRIMARY KEY(id_circuit, id_client)
);

-------------*VUES*-------------

CREATE VIEW plus_consulte AS
SELECT C.id_circuit, C.niveau_difficulte, P.montant, P.type, COUNT(C.id_circuit) AS Nb_Consultations
FROM Circuit AS C INNER JOIN Prix AS P ON C.id_prix=P.id_prix
INNER JOIN Consulter AS Co ON C.id_circuit=Co.id_circuit
GROUP BY C.id_circuit, P.montant, P.type
ORDER BY Nb_Consultations DESC;

CREATE VIEW plus_cherche AS
SELECT C.id_circuit, C.niveau_difficulte, P.montant, P.type, COUNT(C.id_circuit) AS Nb_Cherche
FROM Circuit AS C INNER JOIN Prix AS P ON C.id_prix=P.id_prix
INNER JOIN Chercher AS Co ON C.id_circuit=Co.id_circuit
GROUP BY C.id_circuit, P.montant, P.type
ORDER BY Nb_Cherche DESC;

CREATE VIEW nb_reservations AS
SELECT to_char(date_reservation, 'mm/yyyy') AS MOIS, count(*) AS nb_reservations
FROM Reservation GROUP BY 1 ORDER BY 1;

CREATE VIEW circuits_reserves_age AS
SELECT Cir.id_circuit, C.age, COUNT(Cir.id_circuit) AS nb_circuit_reserve
FROM Client AS C INNER JOIN Reservation AS R ON C.id_client=R.id_client
INNER JOIN Circuit AS Cir ON Cir.id_circuit=R.id_circuit
GROUP BY Cir.id_circuit, C.age
ORDER BY C.age DESC;

