-------------*INSERTION DANS LES TABLES*-------------

INSERT INTO Pays VALUES ('FR', 'France');
INSERT INTO Pays VALUES ('DE', 'Allemagne');
INSERT INTO Pays VALUES ('ES', 'Espagne');
INSERT INTO Pays VALUES ('GB', 'Angleterre');
INSERT INTO Pays VALUES ('IT', 'Italie');
INSERT INTO Pays VALUES ('NL', 'Pays Bas');
INSERT INTO Pays VALUES('PL', 'Pologne');
INSERT INTO Pays VALUES('CZ', 'Republique tcheque');

INSERT INTO Ville VALUES (1, 'Lille', 'FR');
INSERT INTO Ville VALUES (2, 'Paris', 'FR');
INSERT INTO Ville VALUES (3, 'Toulouse', 'FR');
INSERT INTO Ville VALUES (4, 'Berlin', 'DE');
INSERT INTO Ville VALUES (5, 'Grenade', 'ES');
INSERT INTO Ville VALUES (6, 'Seville', 'ES');
INSERT INTO Ville VALUES (7, 'London', 'GB');
INSERT INTO Ville VALUES (8, 'Meran', 'IT');
INSERT INTO Ville VALUES (9, 'Langweer', 'NL');
INSERT INTO Ville VALUES (10, 'Varsovie', 'PL');
INSERT INTO Ville VALUES (11, 'Cracovie', 'PL');
INSERT INTO Ville VALUES (12, 'Milan', 'IT');
INSERT INTO Ville VALUES (13, 'Palerme', 'IT');
INSERT INTO Ville VALUES (14, 'Barcelona', 'ES');
INSERT INTO Ville VALUES (15, 'Amsterdam', 'NL');
INSERT INTO Ville VALUES (16, 'Munich', 'DE');
INSERT INTO Ville VALUES (17, 'Venise', 'IT');
INSERT INTO Ville VALUES (18, 'Les Contamines-Montjoie', 'FR');

INSERT INTO Logement VALUES(1, 1, 'hotel');
INSERT INTO Logement VALUES(2, 1, 'chez_particulier');
INSERT INTO Logement VALUES(3, 2, 'chez_particulier');
INSERT INTO Logement VALUES(4, 5, 'hotel');
INSERT INTO Logement VALUES(5, 5, 'chez_particulier');
INSERT INTO Logement VALUES(6, 8, 'hotel');
INSERT INTO Logement VALUES(7, 9, 'chez_particulier');
INSERT INTO Logement VALUES(8, 6, 'hotel');
INSERT INTO Logement VALUES(9, 4, 'hotel');
INSERT INTO Logement VALUES(10, 7, 'chez_particulier');
INSERT INTO Logement VALUES(11, 4, 'chez_particulier');
INSERT INTO Logement VALUES(12, 10, 'hotel');
INSERT INTO Logement VALUES(13, 11, 'chez_particulier');
INSERT INTO Logement VALUES(14, 13, 'hotel');
INSERT INTO Logement VALUES(15, 12, 'chez_particulier');
INSERT INTO Logement VALUES(16, 14, 'hotel');
INSERT INTO Logement VALUES(17, 15, 'hotel');
INSERT INTO Logement VALUES(18, 16, 'chez_particulier');
INSERT INTO Logement VALUES(19, 18, 'hotel');

INSERT INTO Prix VALUES(1, 650, 'standard');
INSERT INTO Prix VALUES(2, 750, 'eleve');
INSERT INTO Prix VALUES(3, 550, 'standard');
INSERT INTO Prix VALUES(4, 700, 'standard');
INSERT INTO Prix VALUES(5, 850, 'eleve');
INSERT INTO Prix VALUES(6, 750, 'eleve');

INSERT INTO Accompagnateur VALUES(0, 'NULL', 'NULL', 15);
INSERT INTO Accompagnateur VALUES(1, 'Vallee', 'Crystelle', 10);
INSERT INTO Accompagnateur VALUES(2, 'Jam', 'Arnaud', 13);
INSERT INTO Accompagnateur VALUES(3, 'Mercier', 'Arnaud', 15);
INSERT INTO Accompagnateur VALUES(4, 'Bauer', 'Marie', 20);
INSERT INTO Accompagnateur VALUES(5, 'Colombo', 'Marc', 10);
INSERT INTO Accompagnateur VALUES(6, 'Smith', 'Joseph', 5);
INSERT INTO Accompagnateur VALUES(7, 'Fernandez', 'Mario', 18);

INSERT INTO Activite VALUES(1, 'Ski');
INSERT INTO Activite VALUES(2, 'Alpinisme');
INSERT INTO Activite VALUES(3, 'Parachutisme');
INSERT INTO Activite VALUES(4, 'Visite Musee');
INSERT INTO Activite VALUES(5, 'Randonnee');
INSERT INTO Activite VALUES(6, 'Brasserie');
INSERT INTO Activite VALUES(7, 'Visite Zoo');
INSERT INTO Activite VALUES(8, 'Visite plage');
INSERT INTO Activite VALUES(9, 'Parc attractions');
INSERT INTO Activite VALUES(10, 'Restaurant');
INSERT INTO Activite VALUES(11, 'Cyclisme');
INSERT INTO Activite VALUES(12, 'Tyrolienne');
INSERT INTO Activite VALUES(13, 'Theatre');

INSERT INTO Circuit VALUES(1, '2020/12/31', 7, 15, 1, 2);
INSERT INTO Circuit VALUES(2, '2020/08/15', 5, 20, 4, 1);
INSERT INTO Circuit VALUES(3, '2021/01/31', 8, 15, 2, 3);
INSERT INTO Circuit VALUES(4, '2020/07/25', 10, 15, 3, 5);
INSERT INTO Circuit VALUES(5, '2021/04/15', 7, 10, 1, 6);
INSERT INTO Circuit VALUES(6, '2020/12/31', 4, 10, 5, 5);
INSERT INTO Circuit VALUES(7, '2020/10/30', 8, 20, 4, 2);
INSERT INTO Circuit VALUES(8, '2021/01/31', 8, 15, 2, 3);
INSERT INTO Circuit VALUES(9, '2020/05/25', 10, 15, 3, 6);
INSERT INTO Circuit VALUES(10, '2021/04/15', 7, 10, 1, 4);
INSERT INTO Circuit VALUES(11, '2020/12/10', 4, 6, 6, 4);

INSERT INTO Etape VALUES(1, 1, 3, 'bus');
INSERT INTO Etape VALUES(2, 1, 1, 'bus');
INSERT INTO Etape VALUES(3, 1, 9, 'avion');
INSERT INTO Etape VALUES(4, 2, 5, 'avion');
INSERT INTO Etape VALUES(5, 2, 8, 'bus');
INSERT INTO Etape VALUES(6, 3, 16, 'voiture');
INSERT INTO Etape VALUES(7, 3, 6, 'voiture');
INSERT INTO Etape VALUES(8, 3, 15, 'bus');
INSERT INTO Etape VALUES(9, 3, 18, 'avion');
INSERT INTO Etape VALUES(10, 4, 14, 'avion');
INSERT INTO Etape VALUES(11, 4, 13, 'bus');
INSERT INTO Etape VALUES(12, 4, 12, 'bus');
INSERT INTO Etape VALUES(13, 4, 9, 'bus');
INSERT INTO Etape VALUES(14, 5, 18, 'voiture');
INSERT INTO Etape VALUES(15, 5, 8, 'bus');
INSERT INTO Etape VALUES(16, 5, 9, 'voiture');
INSERT INTO Etape VALUES(17, 6, 15, 'voiture');
INSERT INTO Etape VALUES(18, 6, 18, 'avion');
INSERT INTO Etape VALUES(19, 7, 7, 'voiture');
INSERT INTO Etape VALUES(20, 7, 8, 'avion');
INSERT INTO Etape VALUES(21, 7, 1, 'bus');
INSERT INTO Etape VALUES(22, 7, 2, 'bus');
INSERT INTO Etape VALUES(23, 8, 4, 'bus');
INSERT INTO Etape VALUES(24, 8, 13, 'voiture');
INSERT INTO Etape VALUES(25, 9, 13, 'avion');
INSERT INTO Etape VALUES(26, 9, 14, 'avion');
INSERT INTO Etape VALUES(27, 9, 16, 'voiture');
INSERT INTO Etape VALUES(28, 9, 5, 'voiture');
INSERT INTO Etape VALUES(29, 9, 8, 'bus');
INSERT INTO Etape VALUES(30, 10, 9, 'voiture');
INSERT INTO Etape VALUES(31, 10, 15, 'avion');
INSERT INTO Etape VALUES(32, 10, 16, 'voiture');
INSERT INTO Etape VALUES(33, 11, 16, 'bus');


INSERT INTO Client VALUES(1, 'Lopez', 'Chabelo', '6 B Rue Winston Churchill 60200', 0633452176, 20);
INSERT INTO Client VALUES(2, 'Barrera', 'Humberto', '6 B Rue Winston Churchill 60200', 0633379076, 21);
INSERT INTO Client VALUES(3, 'Gilles', 'Savinien', '9 Rue de la Justice 60200', 0633876045, 30);
INSERT INTO Client VALUES(4, 'Lecuyer', 'Chloe', 'Rue des Pierres 1-37 1000', 0620370837, 80);
INSERT INTO Client VALUES(5, 'Lemaitre', 'Antoine', '16-18 Rue Winston Churchill 60200', 0732533889, 18);
INSERT INTO Client VALUES(6, 'Simons', 'Caroline', '330 Rue Washington 7102', 976352165, 18);
INSERT INTO Client VALUES(7, 'Zinner', 'Nick', '450 Avenue 5 10018', 480974204, 38);
INSERT INTO Client VALUES(8, 'Turunen', 'Tarja', 'Marcius 15 ter 1 1056', 269875365, 57);
INSERT INTO Client VALUES(9, 'Curtis', 'Ian', '22 Rue Montrose G11RE', 473698558, 25);
INSERT INTO Client VALUES(10, 'Castrejon', 'Mariand', '93 Avenue Cuauhtemoc 62448', 558796453, 25);
INSERT INTO Client VALUES(11, 'Laveix', 'Adrien', '26-56 Rue des Marchands 31000', 0725302247, 45);
INSERT INTO Client VALUES(12, 'Zhang', 'Wei', '26 Dong Jie 747000', 356894752, 18);
INSERT INTO Client VALUES(13, 'Lee-Orzolek', 'Karen', '514-500 Rue Crete 75203', 963874050, 50);
INSERT INTO Client VALUES(14, 'Chavez', 'Andrea', '1216 Rue Sur 107 15970', 555456894, 20);
INSERT INTO Client VALUES(15, 'Gonzales', 'Christian', '45-75 Rue Esperanza 7560801', 547895600, 80);

INSERT INTO Reservation VALUES(1, 4, 1, '2020/01/14', 3, 'reserve');
INSERT INTO Reservation VALUES(2, 4, 4, '2020/01/14', 2, 'reserve');
INSERT INTO Reservation VALUES(3, 5, 1, '2020/03/18', 3, 'partiellement_paye');
INSERT INTO Reservation VALUES(4, 5, 6, '2020/05/27', 3, 'reserve');
INSERT INTO Reservation VALUES(5, 1, 8, '2020/05/24', 3, 'totallement_paye');
INSERT INTO Reservation VALUES(6, 13, 10, '2020/01/14', 3, 'partiellement_paye');
INSERT INTO Reservation VALUES(7, 15, 1, '2020/03/17', 1, 'reserve');

--Cicuit 1--
	--1--
INSERT INTO PlanActivite VALUES(1, 5, 1, 1, '2020/12/31', '2021/01/01');
	--2--
INSERT INTO PlanActivite VALUES(2, 10, 2, 2, '2021/01/03', '2021/01/04');
	--3--
INSERT INTO PlanActivite VALUES(3, 6, 3, 3, '2021/01/06', '2021/01/07');
--Cicuit 2--
	--4--
INSERT INTO PlanActivite VALUES(4, 1, 4, 5, '2020/08/16', '2020/08/17');
INSERT INTO PlanActivite VALUES(5, 2, 4, 7, '2020/08/20', '2020/08/21');
	--5--
INSERT INTO PlanActivite VALUES(6, 7, 5, 6, '2020/08/30', '2020/08/31');
INSERT INTO PlanActivite VALUES(7, 9, 5, 5, '2020/09/05', '2020/09/06');
INSERT INTO PlanActivite VALUES(8, 13, 5, 0, '2020/09/07', '2020/09/08');
--Cicuit 3--
	--6--
INSERT INTO PlanActivite VALUES(9, 4, 6, 4, '2021/01/05', '2021/01/06');	
	--7--
INSERT INTO PlanActivite VALUES(10, 4, 7, 5, '2021/01/10', '2021/01/15');	
	--9--
INSERT INTO PlanActivite VALUES(11, 4, 9, 1, '2021/02/01', '2021/02/02');
INSERT INTO PlanActivite VALUES(12, 6, 9, 0, '2021/02/02', '2021/02/03');
--Cicuit 4--	
	--10--
INSERT INTO PlanActivite VALUES(13, 13, 10, 0, '2020/07/30', '2020/07/31');	
	--11--
	--12--
INSERT INTO PlanActivite VALUES(14, 4, 12, 2, '2020/08/01', '2020/08/02');	
	--13--
INSERT INTO PlanActivite VALUES(15, 4, 13, 7, '2020/08/16', '2020/08/17');
INSERT INTO PlanActivite VALUES(16, 10, 13, 0, '2020/08/17', '2020/08/18');
--Cicuit 5--	
	--14--
INSERT INTO PlanActivite VALUES(17, 7, 14, 3, '2021/04/16', '2021/04/17');
	--15--
	--16--
INSERT INTO PlanActivite VALUES(18, 4, 16, 7, '2021/05/06', '2021/05/07');
INSERT INTO PlanActivite VALUES(19, 13, 16, 0, '2021/05/07', '2021/05/08');
--Cicuit 6--
	--17--
INSERT INTO PlanActivite VALUES(20, 4, 17, 2, '2021/01/05', '2021/01/06');
INSERT INTO PlanActivite VALUES(21, 4, 17, 2, '2021/01/07', '2021/01/08');	
	--18--
INSERT INTO PlanActivite VALUES(22, 4, 18, 3, '2021/01/15', '2021/01/16');
INSERT INTO PlanActivite VALUES(23, 4, 18, 3, '2021/01/18', '2021/01/19');	
--Cicuit 7--
	--19--
INSERT INTO PlanActivite VALUES(24, 10, 19, 0, '2020/11/01', '2020/11/02');	
	--20--
INSERT INTO PlanActivite VALUES(25, 4, 20, 5, '2020/11/15', '2020/11/16');
INSERT INTO PlanActivite VALUES(26, 10, 20, 5, '2020/11/18', '2020/11/19');
	--21--
INSERT INTO PlanActivite VALUES(27, 4, 21, 3, '2020/12/15', '2020/12/16');
INSERT INTO PlanActivite VALUES(28, 4, 21, 3, '2020/12/18', '2020/12/19');
	--22--
--Cicuit 8--	
	--23--
INSERT INTO PlanActivite VALUES(29, 4, 23, 6, '2021/01/05', '2021/01/06');
INSERT INTO PlanActivite VALUES(30, 4, 23, 6, '2021/01/08', '2021/01/09');
	--24--
INSERT INTO PlanActivite VALUES(31, 4, 24, 7, '2021/02/05', '2021/02/06');
INSERT INTO PlanActivite VALUES(32, 7, 24, 7, '2021/02/08', '2021/02/09');
--Cicuit 9--
	--25--
INSERT INTO PlanActivite VALUES(33, 7, 25, 6, '2020/05/27', '2020/05/28');
	--26--
INSERT INTO PlanActivite VALUES(34, 13, 26, 0, '2020/06/05', '2020/06/06');
	--27--
INSERT INTO PlanActivite VALUES(35, 4, 27, 3, '2020/06/15', '2020/06/16');	
	--28--
INSERT INTO PlanActivite VALUES(36, 4, 28, 3, '2020/06/25', '2020/06/26');	
	--29--
INSERT INTO PlanActivite VALUES(37, 4, 29, 3, '2020/07/07', '2020/07/08');	
--Cicuit 9--
	--30--
INSERT INTO PlanActivite VALUES(38, 10, 30, 0, '2020/04/17', '2020/04/17');
	--31--
INSERT INTO PlanActivite VALUES(39, 10, 31, 0, '2020/04/30', '2020/04/30');	
	--32--
INSERT INTO PlanActivite VALUES(40, 10, 32, 0, '2020/05/15', '2020/05/15');	
--Cicuit 9--
	--33--
INSERT INTO PlanActivite VALUES(41, 2, 33, 6, '2020/12/11', '2020/12/14');
INSERT INTO PlanActivite VALUES(42, 11, 33, 6, '2020/12/20', '2020/12/23');

INSERT INTO Equipments VALUES(1, 1, 'Ski');
INSERT INTO Equipments VALUES(2, 2, 'Harnais de sécurité pour alpinisme');
INSERT INTO Equipments VALUES(3, 2, 'Corde');
INSERT INTO Equipments VALUES(4, 2, 'Piolet');
INSERT INTO Equipments VALUES(5, 2, 'Casque');
INSERT INTO Equipments VALUES(6, 2, 'Bottes');
INSERT INTO Equipments VALUES(7, 3, 'Parachute');
INSERT INTO Equipments VALUES(8, 3, 'Casque');
INSERT INTO Equipments VALUES(9, 3, 'Harnais de sécurité pour parachutisme');
INSERT INTO Equipments VALUES(10, 11, 'Casque');
INSERT INTO Equipments VALUES(11, 11, 'Harnais de sécurité pour cyclisme');
INSERT INTO Equipments VALUES(12, 11, 'Vélo');
INSERT INTO Equipments VALUES(13, 12, 'Casque');
INSERT INTO Equipments VALUES(14, 12, 'Harnais de sécurité pour tyrolienne');

INSERT INTO Consulter VALUES(1,4);
INSERT INTO Consulter VALUES(1,8);
INSERT INTO Consulter VALUES(1,13);
INSERT INTO Consulter VALUES(1,15);
INSERT INTO Consulter VALUES(2,6);
INSERT INTO Consulter VALUES(2,7);
INSERT INTO Consulter VALUES(2,12);
INSERT INTO Consulter VALUES(3,1);
INSERT INTO Consulter VALUES(3,2);
INSERT INTO Consulter VALUES(4,5);
INSERT INTO Consulter VALUES(4,12);
INSERT INTO Consulter VALUES(4,10);
INSERT INTO Consulter VALUES(5,9);
INSERT INTO Consulter VALUES(5,5);
INSERT INTO Consulter VALUES(6,11);
INSERT INTO Consulter VALUES(6,9);
INSERT INTO Consulter VALUES(6,11);
INSERT INTO Consulter VALUES(7,11);
INSERT INTO Consulter VALUES(8,15);

INSERT INTO Chercher VALUES(1, 1);
INSERT INTO Chercher VALUES(1, 2);
INSERT INTO Chercher VALUES(1, 3);
INSERT INTO Chercher VALUES(1, 7);
INSERT INTO Chercher VALUES(1, 9);
INSERT INTO Chercher VALUES(2, 10);
INSERT INTO Chercher VALUES(2, 5);
INSERT INTO Chercher VALUES(3, 10);
INSERT INTO Chercher VALUES(3, 7);
INSERT INTO Chercher VALUES(3, 8);
INSERT INTO Chercher VALUES(4, 8);
INSERT INTO Chercher VALUES(5, 15);
INSERT INTO Chercher VALUES(5, 14);
INSERT INTO Chercher VALUES(5, 5);
INSERT INTO Chercher VALUES(6, 14);
INSERT INTO Chercher VALUES(6, 13);
INSERT INTO Chercher VALUES(6, 8);
INSERT INTO Chercher VALUES(6, 6);
INSERT INTO Chercher VALUES(7, 10);
INSERT INTO Chercher VALUES(7, 4);
INSERT INTO Chercher VALUES(7, 3);
INSERT INTO Chercher VALUES(8, 10);
INSERT INTO Chercher VALUES(8, 1);
INSERT INTO Chercher VALUES(9, 11);
INSERT INTO Chercher VALUES(9, 13);
INSERT INTO Chercher VALUES(9, 12);
INSERT INTO Chercher VALUES(10, 11);
INSERT INTO Chercher VALUES(10, 7);

INSERT INTO Facture VALUES(1, 4);
INSERT INTO Facture VALUES(2, 4);
INSERT INTO Facture VALUES(3, 5);
INSERT INTO Facture VALUES(4, 5);
INSERT INTO Facture VALUES(5, 1);
INSERT INTO Facture VALUES(6, 13);
INSERT INTO Facture VALUES(7, 15);

INSERT INTO Frais VALUES (1, 1, 1, 'reservation');
INSERT INTO Frais VALUES (2, 1, 1, 'location');
INSERT INTO Frais VALUES (3, 2, 2, 'reservation');
INSERT INTO Frais VALUES (4, 2, 2, 'location');
INSERT INTO Frais VALUES (5, 3, 3, 'reservation');
INSERT INTO Frais VALUES (6, 3, 3, 'location');
INSERT INTO Frais VALUES (7, 4, 4, 'reservation');
INSERT INTO Frais VALUES (8, 4, 4, 'location');
INSERT INTO Frais VALUES (9, 5, 5, 'reservation');
INSERT INTO Frais VALUES (10, 5, 5, 'location');
INSERT INTO Frais VALUES (11, 6, 6, 'reservation');
INSERT INTO Frais VALUES (12, 6, 6, 'location');
INSERT INTO Frais VALUES (13, 7, 7, 'reservation');

INSERT INTO Commentaire VALUES(1, 14, NULL , 5);
INSERT INTO Commentaire VALUES(1, 4, 'Bon Circuit' , 5);
INSERT INTO Commentaire VALUES(8, 1, 'ca a ete tres amusant' , 5);
INSERT INTO Commentaire VALUES(6, 5, 'Je suis decu de ce circuit' , 3);
INSERT INTO Commentaire VALUES(10, 14, NULL , 4);
