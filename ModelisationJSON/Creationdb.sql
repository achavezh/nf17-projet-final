CREATE DATABASE agencejs WITH OWNER postgres;

----------------**CREATION DES TABLES**----------------

CREATE TABLE Prix(
id_prix SERIAL PRIMARY KEY,
montant FLOAT,
type VARCHAR NOT NULL,
CHECK(type='standard' OR type='eleve'),
CHECK(montant > 0));

CREATE TABLE Circuit(
id_circuit SERIAL PRIMARY KEY,
date_depart DATE NOT NULL,
duree INTEGER,
nb_participantes INTEGER,
niveau_difficulte INTEGER, 
etape JSON NOT NULL,
id_prix INTEGER NOT NULL,
CHECK (duree>0 AND nb_participantes>0),
FOREIGN KEY (id_prix) REFERENCES Prix(id_prix));

CREATE TABLE Client(
id_client SERIAL PRIMARY KEY,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL,
adresse VARCHAR NOT NULL,
telephone INTEGER NOT NULL,
age INTEGER NOT NULL,
CHECK(age>=18));

CREATE TABLE Reservation(
id_reservation SERIAL PRIMARY KEY, 
id_client INTEGER NOT NULL,
id_circuit INTEGER NOT NULL,
date_reservation DATE NOT NULL,
nb_accompagnates INTEGER,
frais JSON NOT NULL,
status VARCHAR NOT NULL, 
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
CHECK(status='reserve' OR status='partiellement_paye' OR status='totallement_paye'),
CHECK (nb_accompagnates >= 0));

CREATE TABLE Facture(
id_facture SERIAL PRIMARY KEY,
id_client INTEGER NOT NULL,
id_reservation INTEGER NOT NULL,
FOREIGN KEY(id_reservation) REFERENCES Reservation(id_reservation),
FOREIGN KEY(id_client) REFERENCES Client(id_client));

CREATE TABLE Commentaire(
id_circuit INTEGER NOT NULL,
id_client INTEGER NOT NULL,
commentaire VARCHAR,
notation INTEGER,
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
PRIMARY KEY(id_circuit, id_client),
CHECK(notation>=0 AND notation<=5));

CREATE TABLE Consulter(
id_circuit INTEGER NOT NULL,
id_client INTEGER NOT NULL,
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
PRIMARY KEY(id_circuit, id_client));

CREATE TABLE Chercher(
id_circuit INTEGER NOT NULL,
id_client INTEGER NOT NULL,
FOREIGN KEY(id_client) REFERENCES Client(id_client),
FOREIGN KEY(id_circuit) REFERENCES Circuit(id_circuit),
PRIMARY KEY(id_circuit, id_client));

-----------------**VUES**-----------------
    -----Il y a un type un json tres long à insérer dans le Circuit, donc on va faire des vues; la premiere
    -----contentant le circuit avec le nombre des etapes et d'autres informations concernant au circuit,
    -----après un vue avec les lieux de logement de l'etape de chaque circuit et finalement un vue avec 
    -----les activites par chaque etape du circuit. 

----1----
    CREATE VIEW Circuit_nbEtapes AS
    SELECT c.id_circuit, c.date_depart, c.duree, c.nb_participantes, c.niveau_difficulte, 
    json_array_length(C.etape) AS nb_etapes, c.id_prix FROM Circuit c;
    
    SELECT * FROM Circuit_nbEtapes;

----2----
    CREATE VIEW logement_par_CircuitEtapes AS
    SELECT c.id_circuit, CAST(e->>'nb_etape' AS INTEGER) AS id_etape, e->'logement'->'ville_logement' AS ville_logement,  
    e->'logement'->'pays_logement' AS pays_logement, e->'logement'->'type' AS type_logement
    FROM Circuit c JOIN JSON_ARRAY_ELEMENTS(c.etape) e ON TRUE;
    
    SELECT * FROM logement_par_CircuitEtapes;

----3----
    CREATE VIEW activites_par_CircuitEtapes AS
    SELECT c.id_circuit, CAST(e->>'nb_etape' AS INTEGER) AS id_etape, a->'nom' AS activite, a->'date_initial'
    AS date_initial, a->'date_final' AS date_final, a->'accompagnateur' AS accompagnateur,
    a->'equipments' AS equipments FROM Circuit c JOIN JSON_ARRAY_ELEMENTS(c.etape) e ON TRUE
    JOIN JSON_ARRAY_ELEMENTS(e->'activites') a ON TRUE;
    
    SELECT * FROM activites_par_CircuitEtapes;

---------------**VUES PRINCIPALES**---------------

CREATE VIEW plus_consulte AS
SELECT C.id_circuit, C.niveau_difficulte, P.montant, P.type, COUNT(C.id_circuit) AS Nb_Consultations
FROM Circuit AS C INNER JOIN Prix AS P ON C.id_prix=P.id_prix
INNER JOIN Consulter AS Co ON C.id_circuit=Co.id_circuit
GROUP BY C.id_circuit, P.montant, P.type
ORDER BY Nb_Consultations DESC;

CREATE VIEW plus_cherche AS
SELECT C.id_circuit, C.niveau_difficulte, P.montant, P.type, COUNT(C.id_circuit) AS Nb_Cherche
FROM Circuit AS C INNER JOIN Prix AS P ON C.id_prix=P.id_prix
INNER JOIN Chercher AS Co ON C.id_circuit=Co.id_circuit
GROUP BY C.id_circuit, P.montant, P.type
ORDER BY Nb_Cherche DESC;

CREATE VIEW nb_reservations AS
SELECT to_char(date_reservation, 'mm/yyyy') AS MOIS, count(*) AS nb_reservations
FROM Reservation GROUP BY 1 ORDER BY 1;

CREATE VIEW circuits_reserves_age AS
SELECT Cir.id_circuit, C.age, COUNT(Cir.id_circuit) AS nb_circuit_reserve
FROM Client AS C INNER JOIN Reservation AS R ON C.id_client=R.id_client
INNER JOIN Circuit AS Cir ON Cir.id_circuit=R.id_circuit
GROUP BY Cir.id_circuit, C.age
ORDER BY C.age DESC;
