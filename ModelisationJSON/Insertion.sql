INSERT INTO Prix VALUES(1, 650, 'standard');
INSERT INTO Prix VALUES(2, 750, 'eleve');
INSERT INTO Prix VALUES(3, 550, 'standard');
INSERT INTO Prix VALUES(4, 700, 'standard');
INSERT INTO Prix VALUES(5, 850, 'eleve');
INSERT INTO Prix VALUES(6, 750, 'eleve');

INSERT INTO Circuit VALUES(1, '2020/12/31', 7, 15, 1, '[ {"nb_etape":"1", "logement":
{"ville_logement": "Paris", "pays_logement": "France", "type":"hotel"}, 
"activites": [{"date_initial": "2020/12/31", "date_final": "2021/01/02",
"nom":"Randonnee", "accompagnateur": {"nom": "Vallee", "prenom":"Crystelle", 
"montant_frais": "15"}}, {"date_initial": "2021/01/02", "date_final": "2021/01/03",
"nom":"Cyclisme", "accompagnateur": {"nom": "Jam", "prenom":"Arnaud", 
"montant_frais": "10"}, "equipments": ["casque", "velo"]}], "transport": "avion"}, 

{"nb_etape": "2", "logement":
{"ville_logement": "Lille", "pays_logement": "France", "type":"chez_particulier"},
"activites": [{"date_initial": "2021/01/05", "date_final": "2021/01/06",
"nom":"Theatre", "accompagnateur": {"nom": "Vallee", "prenom":"Crystelle",
"montant_frais": "20"}}, {"date_initial": "2021/01/06", "date_final": "2021/01/07",
"nom":"Brasserie", "accompagnateur": {"nom": "Mercier", "prenom":"Arnaud", "montant_frais": "10"}}],
"transport":"bus"}]', 2);

INSERT INTO Circuit VALUES(2, '2020/12/15', 8, 20, 4, '[ 
{"nb_etape":"1", "logement":
{"ville_logement": "Aoste", "pays_logement": "Italie", "type":"chez_particulier"}, 
"activites": [{"date_initial": "2020/12/15", "date_final": "2020/12/17",
"nom":"Parachutisme", "accompagnateur": {"nom": "Fernandez", "prenom":"Mario", 
"montant_frais": "18"}, "equipments": ["harnais pour parachute", "parachute", "casque"]},
{"date_initial": "2020/12/19", "date_final": "2021/12/20",
"nom":"Cyclisme", "accompagnateur": {"nom": "Bauer", "prenom":"Marie", 
"montant_frais": "20"}, "equipments": ["casque", "velo"]}], "transport": "avion"}, 

{"nb_etape": "2", "logement":
{"ville_logement": "Grenade", "pays_logement": "Espagne", "type":"hotel"},
"activites": [{"date_initial": "2020/12/20", "date_final": "2020/12/22",
"nom":"Tyrolienne", "accompagnateur": {"nom": "Sanchez", "prenom":"Mariana",
"montant_frais": "20"} ,"equipments": ["harnais tyrolienne", "casque"]}],
"transport":"voiture"}, 

{"nb_etape":"3", "logement":
{"ville_logement": "London", "pays_logement": "Angleterre", "type":"chez_particulier"}, 
"activites": [{"date_initial": "2020/12/22", "date_final": "2020/12/24",
"nom":"Restaurante" }], "transport": "avion"}]', 5);

INSERT INTO Circuit VALUES(3, '2020/07/25', 4, 10, 0, '[ 
{"nb_etape":"1", "logement":
{"ville_logement": "Barcelone", "pays_logement": "Espagne", "type":"hotel"}, 
"activites": [{"date_initial": "2020/07/25", "date_final": "2020/07/26",
"nom":"Theatre"},
{"date_initial": "2020/07/26", "date_final": "2020/07/28",
"nom":"Visite Zoo"}, {"date_initial": "2020/07/28", "date_final": "2020/07/29",
"nom":"Brasserie"}], "transport": "avion"}]', 4);

INSERT INTO Circuit VALUES(4, '2021/01/31', 8, 18, 3, '[ 
{"nb_etape":"1", "logement":
{"ville_logement": "Grenade", "pays_logement": "Espagne", "type":"chez_particulier"}, 
"activites": [{"date_initial": "2021/01/31", "date_final": "2021/02/02",
"nom":"ski", "accompagnateur": {"nom": "Sanchez", "prenom":"Mariana", 
"montant_frais": "18"}, "equipments": ["ski"]},
{"date_initial": "2021/02/02", "date_final": "2021/02/04",
"nom":"Alpinisme", "accompagnateur": {"nom": "Fernandez", "prenom":"Mario", 
"montant_frais": "20"}, "equipments": ["harnais alpinisme"]}], "transport": "bus"}, 

{"nb_etape": "2", "logement":
{"ville_logement": "Venise", "pays_logement": "Italie", "type":"hotel"},
"activites": [{"date_initial": "2021/02/04", "date_final": "2021/02/06",
"nom":"Parc d attractions"}, {"date_initial": "2021/02/06", "date_final": "2021/02/08",
"nom":"Visite musee", "accompagnateur":{"nom": "Colombo", "prenom":"Marc", 
"montant_frais": "10"}}],
"transport":"avion"}]', 4);

INSERT INTO Circuit VALUES(5, '2020/08/15', 8, 20, 1, '[ 
{"nb_etape":"1", "logement":
{"ville_logement": "Amsterdam", "pays_logement": "Pays-Bas", "type":"hotel"}, 
"activites": [{"date_initial": "2020/08/15", "date_final": "2020/08/17",
"nom":"Visite Musee"},
{"date_initial": "2020/08/17", "date_final": "2020/08/19",
"nom":"Restaurant"},{"date_initial": "2020/08/19", "date_final": "2020/08/21",
"nom":"Brasserie"}], "transport": "avion"}, 

{"nb_etape": "2", "logement":
{"ville_logement": "Lille", "pays_logement": "France", "type":"chez_particulier"},
"activites": [{"date_initial": "2020/08/21", "date_final": "2020/08/22",
"nom":"Visite Zoo"}], "transport":"voiture"}]', 3);

INSERT INTO Client VALUES(1, 'Lopez', 'Chabelo', '6 B Rue Winston Churchill 60200', 0633452176, 20);
INSERT INTO Client VALUES(2, 'Barrera', 'Humberto', '6 B Rue Winston Churchill 60200', 0633379076, 21);
INSERT INTO Client VALUES(3, 'Gilles', 'Savinien', '9 Rue de la Justice 60200', 0633876045, 30);
INSERT INTO Client VALUES(4, 'Lecuyer', 'Chloe', 'Rue des Pierres 1-37 1000', 0620370837, 80);
INSERT INTO Client VALUES(5, 'Lemaitre', 'Antoine', '16-18 Rue Winston Churchill 60200', 0732533889, 18);
INSERT INTO Client VALUES(6, 'Simons', 'Caroline', '330 Rue Washington 7102', 976352165, 18);
INSERT INTO Client VALUES(7, 'Zinner', 'Nick', '450 Avenue 5 10018', 480974204, 38);
INSERT INTO Client VALUES(8, 'Turunen', 'Tarja', 'Marcius 15 ter 1 1056', 269875365, 57);
INSERT INTO Client VALUES(9, 'Curtis', 'Ian', '22 Rue Montrose G11RE', 473698558, 25);
INSERT INTO Client VALUES(10, 'Castrejon', 'Mariand', '93 Avenue Cuauhtemoc 62448', 558796453, 25);
INSERT INTO Client VALUES(11, 'Laveix', 'Adrien', '26-56 Rue des Marchands 31000', 0725302247, 45);
INSERT INTO Client VALUES(12, 'Zhang', 'Wei', '26 Dong Jie 747000', 356894752, 18);
INSERT INTO Client VALUES(13, 'Lee-Orzolek', 'Karen', '514-500 Rue Crete 75203', 963874050, 50);
INSERT INTO Client VALUES(14, 'Chavez', 'Andrea', '1216 Rue Sur 107 15970', 555456894, 20);
INSERT INTO Client VALUES(15, 'Gonzales', 'Christian', '45-75 Rue Esperanza 7560801', 547895600, 80);

INSERT INTO Reservation VALUES(1, 4, 3, '2020/01/14', 3,'["reservation"]','reserve');
INSERT INTO Reservation VALUES(2, 15, 3, '2020/01/14', 2,'["reservation"]' ,'reserve');
INSERT INTO Reservation VALUES(3, 5, 1, '2020/03/18', 3,'["reservation","location"]' ,'partiellement_paye');
INSERT INTO Reservation VALUES(4, 6, 1, '2020/05/27', 4,'["reservation","location"]','reserve');
INSERT INTO Reservation VALUES(5, 5, 2, '2020/03/18', 3,'["reservation","location"]','totallement_paye');
INSERT INTO Reservation VALUES(6, 4, 4, '2020/05/27', 1,'["reservation","location"]','totallement_paye');
INSERT INTO Reservation VALUES(7, 9, 5, '2020/01/14', 3, '["reservation"]','partiellement_paye');
INSERT INTO Reservation VALUES(8, 10, 5, '2020/03/18', 5,'["reservation"]' ,'totallement_paye');

INSERT INTO Facture VALUES (1, 4, 1);
INSERT INTO Facture VALUES (2, 15, 2);
INSERT INTO Facture VALUES (3, 5, 3);
INSERT INTO Facture VALUES (4, 6, 4);
INSERT INTO Facture VALUES (5, 5, 5);
INSERT INTO Facture VALUES (6, 4, 6);
INSERT INTO Facture VALUES (7, 9, 7);
INSERT INTO Facture VALUES (8, 10, 8);

INSERT INTO Consulter VALUES(1,1);
INSERT INTO Consulter VALUES(1,2);
INSERT INTO Consulter VALUES(1,5);
INSERT INTO Consulter VALUES(1,6);
INSERT INTO Consulter VALUES(2,7);
INSERT INTO Consulter VALUES(2,9);
INSERT INTO Consulter VALUES(2,10);
INSERT INTO Consulter VALUES(3,4);
INSERT INTO Consulter VALUES(3,15);
INSERT INTO Consulter VALUES(4,5);
INSERT INTO Consulter VALUES(4,12);
INSERT INTO Consulter VALUES(4,10);
INSERT INTO Consulter VALUES(4,9);
INSERT INTO Consulter VALUES(4,7);
INSERT INTO Consulter VALUES(5,11);
INSERT INTO Consulter VALUES(5,9);
INSERT INTO Consulter VALUES(5,8);
INSERT INTO Consulter VALUES(5,10);
INSERT INTO Consulter VALUES(5,15);

INSERT INTO Chercher VALUES(1, 1);
INSERT INTO Chercher VALUES(1, 2);
INSERT INTO Chercher VALUES(1, 3);
INSERT INTO Chercher VALUES(1, 5);
INSERT INTO Chercher VALUES(1, 9);
INSERT INTO Chercher VALUES(1, 10);
INSERT INTO Chercher VALUES(2, 12);
INSERT INTO Chercher VALUES(2, 14);
INSERT INTO Chercher VALUES(2, 6);
INSERT INTO Chercher VALUES(2, 5);
INSERT INTO Chercher VALUES(3, 4);
INSERT INTO Chercher VALUES(3, 15);
INSERT INTO Chercher VALUES(3, 8);
INSERT INTO Chercher VALUES(4, 3);
INSERT INTO Chercher VALUES(4, 11);
INSERT INTO Chercher VALUES(4, 13);
INSERT INTO Chercher VALUES(5, 11);
INSERT INTO Chercher VALUES(5, 12);
INSERT INTO Chercher VALUES(5, 8);
INSERT INTO Chercher VALUES(5, 3);

INSERT INTO Commentaire VALUES(1, 2, NULL , 5);
INSERT INTO Commentaire VALUES(1, 6, 'Bon Circuit' , 5);
INSERT INTO Commentaire VALUES(3, 8, 'Tres tranquille et interesante' , 5);
INSERT INTO Commentaire VALUES(2, 14, 'Je suis decu de ce circuit' , 3);
INSERT INTO Commentaire VALUES(2, 3, NULL , 4);
