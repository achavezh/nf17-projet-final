# Note de clarification

## Objectif du projet.

Un agénce touristique souhaite d'avoir une base de données ainsi que d'un système pour gestioner tout ce qui est réliée à ses voyages.

L'agénce propose des circuits touristiques à ces clients afin de qu'ils puissent les résérver, les consulter ou les chercher.

Aussi de permettre à l'agénce d'avoir des statistiques sur ses voyages.

Donc l'objectif est de créer cette basse en prennant en compte les besoins de l'agénce.

## Réalisateur.

- CHÁVEZ HERREJÓN Andrea

## Livrables du projet.

**_Livrables documentées_**

- Note de Clarification. 24/05/2020
- Modèle Conceptuel des Données. 24/05/2020
- Modèle Logique des Données. 31/05/2020

**_Livrables informatiques_**

- Creátion de la base de données. 31/05/2020
- Insertion dans la base de données. 31/05/2020
- Selection d'informations dans la base de données, ainsi que des vues. 02/06/2020 

**_Revisión finale_** 09/06/2020.

## Risques:

- Mauvaise modélisation.
- Limitation de temps.
- Mauvaise connaisance sur les transformation d'heritage ou de langage SQL.

## Acteurs ou rôles du projet.

- Gestionnaire du système:
    - Ajouter des circuits.
    - Ajouter des activites, equipments et accompagnateurs.
    - Ajoutes les possibles lieus d'heberguement ainsi que les villes et pays où ils se trouvent.
    - Consulter des commentaires reçus de chaque circuit ainsi que des réservations.
    - Ajouter des prix. 
    - Voir les statistiques sur les circuits. 
- Client: Le client peut:
    - Chercher voyages.
    - Consulter voyages.
    - Faire des réservations.
    - Ajouter d'autres passageurs.
    - Recevoir des factures.
    - Laisser des commentaires pour des Circuits achetés-
- Accompagnateurs: Personne qui est avec le client pendant les activités du voyage, ils peuvent:
    - Consulter son emploi du temps

## Classes et ses propriétés.

**_Classes gérées par la base_**

- Circuit. (Il divisé en Etapes).
- Prix. Classe mère de:
    - PrixStandard
    - PrixEleve
- Étape.
- PlanActivite. (Planifiées dans chaque Etape).
- Activité. (Référencé par PlanActivite).
- Accompagnateur. (Référencé par PlanActivite).
- Logement.
- Ville. (Référencée par Logement).
- Pays. (Réferencée par ville).
- Equipment. (Offert par le type d'activité et loué par le client).
- Frais. Classe mère de:
    - Frais des réservation. (Référencée par Réservation).
    - Frais de location. (Référencée par Logement).
- Facture.
- Client.
- Réservation. (Classe d'association entre Client et Circuit).
- Logement. Classe mère de:
    - Hôtel.
    - ChezParticulier.
- Commentaire. (Classe d'association entre Client et Circuit).

## Héritage.

Pour l'heritage, tous les classes mères qu'on a trouvé sont abstraites, et pour la classe Prix et Logement, on aura un heritage _exclusif_, mais aussi des associations sur la classe mère, donc on peut bien choisir une transformation par _classe mère_.

D'autre côtè on a un heritage _non-exclusif_ pour la classe Frais et il est complet, donc sera mieux d'utiliser aussi une transformation de heritage par _classe mère_.

## Proprietés et contraintes des classes.

**Circuit**

- id_circuit: int {key}. 
- date_depart: Date {not null}
- duree: int {not null}
- nb_participants: int {not null}
- niveau_difficulte: int {not null} (compris entre 0 et 5)
- id_prix: int {not null}

**_Remarque:_** 

- la duree doit être > 0

**Prix**

- id_prix: int {key}
- montant: float {not null}
- type: {standard, eleve} {not null}

**_Remarque:_** 
- On pourra déterminer le période grâce à une méthode qui retourne un bool si nous sommes dans une période spécial.
- Le montant doit être > 0

**Ville**

- id_ville: int {key}
- nom_ville: String {not null}

**Pays**

- code_pays: string {key}
- nom_pays: string {not null}

**_Remarque:_** code_pays est de taille maximal 2.

**Logement**

- id_logement: int {key}
- type: {hotel, chez_particulier}

**Etape** 

- id_etape: int {local key}
- lieu_heberguement: String {not null}
- transport: {avion, bus, voiture}

**Activite**

- id_activite: int {key}
- type_activite: String {unique et not null}

**Accompagnateur**

- id_accompagnateur: int {key}
- nom: string {not null}
- prenom: string {not null}
- montant_frais: float {not null}

**_Remarque:_** frais_logement doit être >= 0, car le logement peut n'avoir des frais.

**PlanActivite**

- date_initial: date {not null}
- date_final: date {not null}

**_Remarques:_** 
- date_initial > date_final
- Cette table a été introduit afin de que chaque accompagnateur peut consulter son Emploi du temps.

**Equipments**

- id_equip: int {key}
- nom_equip: string {not null}

**_Remarque:_** cout doit être > 0.

**Client**

- id_client: int {key}
- nom: string {not null}
- prenom: String {not null}
- addresse: String {not null}
- telephone: int {not null}
- age: int {not null}

**_Remarque:_** L'age doit être >= 18

**Reservation**

- id_reservation: int {key}
- date_reservation: Date {not null}
- nb_accompagnantes: int
- status: {reserve, partiellement_paye, totallement_paye}

On aura des méthodes suivantes:
- float coutTotReservation(): Qui va calculer le montant total de la réservation inclus les frais des accompagnateurs.
- prochainPaiement(): Qui va reprogrammer le prochain paiement afin de que le client n'oublie pas de payer qui doit être completé 15 jours avant le départ.
- float restant(): Qui va calculer le montant restant.

Le nb_accompagnantes est introduit pour laisser le client de lui dire combien des personnes iront avec lui.

**Frais**

- id_frais: int {unique}
- type: {logement, reservation}

**_Remarques:_** 
- On aura un métode qui calcule le montant total des Frais de Location car un Circuit peut avoir plusieurs: float coutTotFraisLocation().
- On aura aussi un méthode qui calcule le montant totale des Frais de Réservation: floatCoutTotReservation()

**Facture**

- id_facture: int {local}

**_Remarques:_** 
- On aura un méthode calculerFrais() qui va calculer le montant totale des frais.

**Commentaire**

- commentaire: String 
- notation: int (compris entre 0 et 5).

Tous les proprietés ou attributes qui n'ont pas **_{not null}_** sont optionelles.

## Associations et compositions.

- **Ville N:1 Pays:** Un pays contient plusieurs Villes, mais un Ville est contenu dans 1 Pays.
- **Ville 1:N Logement:** Un Ville contient plusieurs Logement, mais on a un Logement spécifique ne peut pas exister dans plusieurs Villes.
- **Activite 1:N PlanActivite:** Un Activité apparaitre dans plusieurs plannings differentes.
- **Accompagnateur 1:N PlanActivite:** Un Accompagnateur peut apparaitre dans plusieurs activites.
- **PlanActivite  N:1 Etape:** Un Étape planifiee plusieurs Activités.
- **Activite 1:N Equipments:** Un activité peut necessiter plusieurs Équipments, mais le même equipment ne peut être nécessaire pour plusieurs activités, car ils doivent être uniques.
- **Circuit composée des Etapes: 1:N** Un circuit est composée des Étapes, c'est une composition donc la côté de Circuit a une cardinalité égal à 1.
- **Etape N:1 Logement:** Un étape a la s'heberge dans un Logement, mais un Logement peut être utilisée dans autres Étapes d'autres Circuits.
- **Circuit N:1 Prix:** Un Circuit a un Prix établie, mais un Prix peut apparaitre dans plusieurs Circuits.
- **Client N:M Circuit->Commentaire:** Un Circuit peut avoir plusieurs Commentaires de plusieurs Clients.
- **Client N:M Circuit->Reservation:** Un Client peut faire plusieurs Réservations de plusieurs Circuits différentes.
- **Client N:M Circuit:** Un Client peut chercher un ou plusieurs Circuits et un Circuit peut être cherché par plusieurs Clients.
- **Client N:M Circuit:** Un Client peut consulter plusieurs Circuits et un Circuit peut être consulté par plusieurs Clients.
- **Reservation 1:N Frais:** Un réservation gere des frais de location a partir du Circuit choisi car chaque Circuit a plusieurs Accompagnantes avec des frais différentes et aussi gére des frais de réservation.
- **Facture 1:N Frais:** Un facture contient tous les frais de type Location et Réservation.
- **Client 1:N Facture:** Un client peut recevoir plusieurs Factures de plusieurs Réservation qu'il fait, mais un Facture spécifique correspond à 1 Client.

## Vues.

Comme on a dit on peut créer des vues pour:

- Les Circuits consultés pour tous Client en les ordenant du plus consulté au moins consulté: Ici donc, on va compter les circuits consultés par différentes Clients en montrant le type de prix et le montant, donc on va faire appel aux tables Client, Circuit, Prix et Consulter.
- Les Circuit cherchés pour tous Client en les ordenant du plus cherché au moins cherché: Ici donc, on va compter les circuits cherchés par différentes Clients en montrant le type de prix et le montant, donc on va faire appel aux tables Client, Circuit, Prix et Chercher.
- Le nombre des réservations par mois: Ici on va extraire le mois de toutes les dates_reservation de la table Réservation pour sommer celles qui ont le même date de type 'mm/yyyy', donc on va nécéssiter seulement la table Reservation qui contient les id des circuits réservés.
- Circuits reservees par âge de voyageurs: Ici on va compter les circuits réservés en trouvant l'age des Clients, donc on va faite appel aux tables Client, Circuit et Reservation.



