# Décomposition en 3NF.

Nous avons les relations du MLD suivantes:

- Pays(#code_pays: string(2), nom_pays: string) avec nom_pays NOT NULL

- Ville(#id_ville, id_pays=>Pays, nom_ville: string) avec nom_ville NOT NULL

- Logement(#id_logement: int, id_ville=>Ville, type: {hotel, chez_particulier}) avec type NOT NULL

- Prix(#id_prix: int, montat: float, type: {standard, eleve}) avec etat NOT NULL

- Ciruit(#id_circuit: int, date_depart: Date, duree: int, nb_participants: int, niveau_difficulte: int, id_prix=>Prix) avec date_depart NOT NULL

- Etape(#id_etape: int, id_circuit=>Circuit, id_logement=>Logement, transport: {avion, bus, voiture}) avec transport NOT NULL

- Accompagnateur(#id_accompagnateur: int, nom: string, prenom: string, montant_frais: float) avec nom, prenom, montant_frais NOT NULL

- Activite(#id_activite: int, type_activite: string} avec type_activite UNIQUE et NOT NULL

- Plan Activite(#id_planactivite: int, id_activite=>Activite, id_etape=>Etape, #id_accompagnateur=>Accompagnateur, date_initial: date, date_final: date) avec date_initial et date_final NOT NULL

- Equipments(#id_equip: int, id_activite=>Equipments, nom_equip: string} avec nom_equip NOT NULL.

- Client(#id_client: int, nom: string, prenom: string, adresse: string, telephone: int, age: int) avec nom, prenom, adresse, telephone, age NOT NULL

- Reservation(#id_reservation: int, id_client=>Client, id_circuit=>Circuit, date_reservation: date, nb_accompagnantes: int, status: {reserve, partiellement_paye, totallement_paye}) avec date_reservation et status NOT NULL

- Facture(#id_facture, id_Client=>Client) 

- Frais(#id_frais: int, id_reservation=>Reservation, id_facture=>Facture, type={location, reservation}) avec type  NOT NULL

- Commentaire(id_circuit=>Circuit, id_client=>Client, commentaire: string, notation: int) avec (id_circuit, id_client) key

- Consulter(#id_reservation: int, id_client=>Client) avec (id_reservation, id_client) key

- Chercher(#id_reservation: int, id_client=>Client)  avec (id_reservation, id_client) key

## Fermetures transitives. 

**Pays:**

* code_pays -> nom_pays.

Ici on voit que on n'a qu'une seule clé candidate. 

**Ville:**

* id_ville -> nom_ville
* id_ville -> id_pays(Pays)

Ici on voit qu'il y a une seule clé candidat, id_ville, parce que code_pays ne peut donner le nom de la Ville car il existent plusieurs Pays qui ont le même nom de Ville.

**Logement:**

* id_Logement -> id_ville(Ville)
* id_Logement -> type

Ici on voit qu'il y a une seule clé candidat, parce que c'est le même cas, différentes Villes peuvent avoir le même type de Logement.

**Prix:**

* id_prix -> montant
* id_prix -> type

**Circuit:**

* id_circuit -> date_depart
* id_circuit -> duree
* id_circuit -> nb_participantes
* id_circuit -> niveau_difficulte
* id_circuit -> id_prix(Prix)

Comme peut avoir un même circuit mais avec differentes types de Prix, on doit l'identifier avec un id_client.

**Etape:**

* id_etape -> id_Circuit(Circuit)
* id_etape -> id_logement(Logement)
* id_transport -> type

**Accompagnateur:**

* id_accompagnateur -> nom
* id_accompagnateur -> prenom
* id_accompagnateur -> montant_frais

**Activite:**

* id_activite -> type_activite

**PlanActivite:**

* id_planactivite -> id_etape(Etape)
* id_planactivite -> id_activite(Activite)
* id_planactivite -> date_initial
* id_planactivite -> date_final

Ici on a introduit cette clé artificielle id_planactivite parce que on peut répéter la même activité dans dans différentes étapes, donc on doit avoir une clé pour pouvoir répéter cettes données là.

**Equipment:**

* id_equip -> id_activite(Activite)
* id_equip -> nom_equip

**Client:**

* id_client -> nom
* id_client -> prenom
* id_client -> adresse
* id_client -> telephone
* id_client -> age

**Reservation:**

* id_reservation -> id_client(Client)
* id_reservation -> id_circuit(Circuit)
* id_reservation -> id_date_reservation
* id_reservation -> nb_accompagnantes
* id_reservation -> status

**Facture:**

* id_facture -> id_client(Client)

**Frais:**

* id_frais -> id_reservation(Reservation)
* id_frais -> id_facture(Facture)
* id_frais -> type

**Commentaire:** 

* (id_circuit(Circuit), id_client(Client)) -> commentaire
* (id_circuit(Circuit), id_client(Client)) -> notation

Ici c'est mieux d'utiliser une clé composée parce que le client ne peut laisser qu'un Commentaire par Circuit.

**Consulter:**

* (id_circuit(Circuit), id_client(Client)) -> {}

**Chercher:**

* (id_circuit(Circuit), id_client(Client)) -> {}

## 1NF.

Comme on peut observer toutes les relations ont une clé et les attributs sont atomiques.

## 2NF.

Pour le 2NF, nous sommes en 1NF et tous les attributs sont déterminées par la clé candidate et pas seulement par une partie de la clé.

## 3NF

Pour le 3NF, tous les attributs sont déterminés par la clé candidate, c'est-à-dire que tous les attributs que n'appartient pas à une clé, sont issues d'une clé.